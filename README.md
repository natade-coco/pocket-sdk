# Pocket SDK

Library for accessing data of natadeCOCO mobile app (Pocket) from natadeCOCO content app (App).

![Pocket SDK](doc/pocket-sdk.png)

## Installation

```sh
npm install @natade-coco/pocket-sdk
```

## Version Compatibility

| Pocket SDK | Pocket |
| :-- | :-- |
| 0.15.0+ | 1.19.4+ |

## Usage

```js
import Pocket from '@natade-coco/pocket-sdk';

Pocket.getProfile(['name', 'email'])
  .then(profile => {
    if (profile) {
      const { name, email } = profile;
      console.log(name);  // ex.) 名無しユーザー
      console.log(email);  // ex.) example@natade-coco.com
    }
  })
  .catch(error => {
    console.error(error);  // ex.) userCanceled
  });
```

## Error

| error | description |
| :-- | :-- |
| `badRequest` | The API argument is incorrect. |
| `unavailable` | The API is in unavailable state. |
| `permissionDenied` | User has denied access. |
| `userCanceled` | The operation was interrupted because user canceled it. |
| `incompatible` | Client app is not running on Pocket (react-native-webview). |
| `internalError` | Error has occurred inside SDK. Usually does not occur. |

## API

### `getProfile(params: string[]): Promise<Profile | undefined>`

Get user's profile.

| param | type | description | example |
| :-- | :-- | :-- | :-- |
| `params` | string[] | Specify parameter names you want to get in user's profile. | ['name', 'email'] |

#### `Profile` object

Only include those contained in `params`.

| param | type | example |
| :-- | :-- | :-- |
| `name?` | string | '名無しユーザー' |
| `fullName?` | string \| undefined | 'ナタデココ 太郎' |
| `email?` | string \| undefined | 'example@natade-coco.com' |
| `tel?` | string \| undefined | '012-345-6789' |
| `image?` | string \| undefined | 'https://natade-coco.com/example.jpg' |

### `getEthereumAddress(): Promise<string>`

Gets user's Ethereum address like `0x1a2b....`.
The address is derived from the user's HD wallet (BIP44) with path `m/44'/60'/0'/0/0`.

### `requestSignJWT(serviceType?: string): Promise<string>`

Get JWT signed by Pocket user for the service. The JWT exp is set to 24 hours.

| param | type | description |
| :-- | :-- | :-- |
| `serviceType` | 'AppHubService' \| 'PaymentHubService' | Service type as audience of JWT (default: 'AppHubService') |

### `makeConnection(target: string, path: string, allows: string[]): Promise<string>`

Generate a new DID document associated with the app, and return its URL.

| param | type | description | example |
| :-- | :-- | :-- | :-- |
| `target` | string | DID of the app | 'did:ethr:0x1234...' |
| `path` | string | Path of the app | '/orders/1' |
| `allows` | string[] | DID of users allowed to use `target` app | ['did:ethr:0x5678....'] |

### `openInBrowser(url: string): Promise<void>`

Open the URL in an external web browser.

| param | type | description | example |
| :-- | :-- | :-- | :-- |
| `url` | string | URL to open in browser | 'https://natade-coco.com/' |

### `share(content: ShareContent): Promise<void>`

Share a text or file.

#### `ShareContent`

| param | type | description | example |
| :-- | :-- | :-- | :-- |
| `text?` | string | Text to share | 'Lorem Ipsum' |
| `file?` | File | File object to share | 'https://natade-coco.com/example.png' |

##### `File`

| param | type | description | example |
| :-- | :-- | :-- | :-- |
| `url` | string | File URL | 'https://natade-coco.com/example.png' 'data:image/png;base64,xxxxx' |
| `name?` | string | File name | 'example.png' |
| `type?` | string | File MIME Type | 'image/png' |

### `lockOrientation(orientation: string | null): Promise<void>`

Locks the orientation of the device while the content is displayed.

| param | type | description |
| :-- | :-- | :-- |
| `orientation` | 'portrait' \| 'landscape' \| 'landscapeLeft' \| 'landscapeRight' \| null | Orientation to lock (null: unlock any orientation) |
