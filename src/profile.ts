import { sendGetProfileRequest } from "./message/profile";

export interface Profile {
  name?: string,
  fullName?: string | null,
  email?: string | null,
  tel?: string | null,
  image?: string | null
}

export const getProfile = (params: string[]) => new Promise<Profile | null>((resolve, reject) => {
  sendGetProfileRequest(params)
    .then(response => {
      const { payload, error } = response;
      if (!error && payload) {
        return resolve(payload.profile);
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});
