import { Orientation, ShareContent } from "./entities/utility";
import { sendLockOrientationRequest, sendOpenInBrowserRequest, sendShareRequest } from "./message/utility";

export const openInBrowser = (url: string) => new Promise<void>((resolve, reject) => {
  if (!url) {
    return reject('badRequest');
  }

  sendOpenInBrowserRequest(url)
    .then(response => {
      const { error } = response;
      if (!error) {
        return resolve();
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});

export const share = (content: ShareContent) => new Promise<void>((resolve, reject) => {
  if (!content.text && !content.file) {
    return reject('badRequest');
  }

  sendShareRequest(content)
    .then(response => {
      const { error } = response;
      if (!error) {
        return resolve();
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});

export const lockOrientation = (orientation: Orientation) => new Promise<void>((resolve, reject) => {
  sendLockOrientationRequest(orientation)
    .then(response => {
      const { error } = response;
      if (!error) {
        return resolve();
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
})
