import { sendMakeConnectionRequest } from "./message/connection";

export const makeConnection = (target: string, path: string, allows: string[]) => new Promise<string>((resolve, reject) => {
  if (!target || !path || !allows) {
    return reject('badRequest');
  }

  sendMakeConnectionRequest(target, path, allows)
    .then(response => {
      const { payload, error } = response;
      if (!error && payload) {
        return resolve(payload.url);
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});
