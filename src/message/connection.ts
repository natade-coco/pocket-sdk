import { Message, MessageError, sendRequest } from './index';

export interface MakeConnectionRequest extends Message {
  type: 'request',
  name: 'makeConnection',
  payload: {
    path: string,
    target: string,
    allows: string[]
  }
};
export interface MakeConnectionResponse extends Message {
  type: 'response',
  name: 'makeConnection',
  payload: {
    url: string
  } | null,
  error: MessageError | null
};
export const sendMakeConnectionRequest = (target: string, path: string, allows: string[]) => new Promise<MakeConnectionResponse>((resolve, reject) => {
  const request: MakeConnectionRequest = {
    type: 'request',
    name: 'makeConnection',
    payload: { target, path, allows }
  };
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'makeConnection')) {
        const { payload, error } = message;
        const response: MakeConnectionResponse = {
          type: message.type,
          name: message.name,
          payload: payload || null,
          error: (error as MessageError) || null
        };
        return resolve(response);
      } else {
        return reject('internal');
      }
    })
    .catch(error => reject(error));
});
