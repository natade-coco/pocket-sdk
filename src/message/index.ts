export type MessageType = 'request' | 'response';
export type MessageName = 'getProfile' | 'getEthereumAddress' | 'requestSignJWT'
                        | 'makeConnection' | 'openInBrowser' | 'share' | 'lockOrientation';
export type MessageError = 'badRequest' | 'unavailable' | 'permissionDenied'
                         | 'userCanceled' | 'incompatible' | 'internalError';

export interface Message {
  type: MessageType,
  name: MessageName,
  payload?: any,
  error?: MessageError | null
}

export const sendRequest = (message: Message) => new Promise<Message>((resolve, reject) => {
  if (!('ReactNativeWebView' in window)) {
    return reject(`incompatible`);
  }
  if (message.type !== 'request') {
    return reject(`internalError`);
  }

  const onMessage = (event: any) => {
    const json: Message = JSON.parse(event.data);
    if ((json.name === message.name) && (json.type === 'response')) {
      removeMessageEventListener();
      return resolve(json);
    }
  };
  const addMessageEventListener = () => {
    window.addEventListener('message', onMessage);
    document.addEventListener('message', onMessage);
  };
  const removeMessageEventListener = () => {
    window.removeEventListener('message', onMessage);
    document.removeEventListener('message', onMessage);
  };

  addMessageEventListener();
  (window as any).ReactNativeWebView.postMessage(JSON.stringify(message));
});
