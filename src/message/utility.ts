import { Orientation, ShareContent } from '../entities/utility'
import { Message, MessageError, sendRequest } from './index'

export interface OpenInBrowserRequest extends Message {
  type: 'request',
  name: 'openInBrowser',
  payload: {
    url: string
  }
}
export interface OpenInBrowserResponse extends Message {
  type: 'response',
  name: 'openInBrowser',
  error: MessageError | null
}
export const sendOpenInBrowserRequest = (url: string) => new Promise<OpenInBrowserResponse>((resolve, reject) => {
  const request: OpenInBrowserRequest = {
    type: 'request',
    name: 'openInBrowser',
    payload: { url }
  }
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'openInBrowser')) {
        const { error } = message
        const response: OpenInBrowserResponse = {
          type: message.type,
          name: message.name,
          error: (error as MessageError) || null
        }
        return resolve(response)
      } else {
        return reject('internal')
      }
    })
    .catch(error => reject(error))
})

export interface ShareRequest extends Message {
  type: 'request',
  name: 'share',
  payload: {
    content: ShareContent
  }
}
export interface ShareResponse extends Message {
  type: 'response',
  name: 'share',
  error: MessageError | null
}
export const sendShareRequest = (content: ShareContent) => new Promise<ShareResponse>((resolve, reject) => {
  const request: ShareRequest = {
    type: 'request',
    name: 'share',
    payload: { content }
  }
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'share')) {
        const { error } = message
        const response: ShareResponse = {
          type: message.type,
          name: message.name,
          error: (error as MessageError) || null
        }
        return resolve(response)
      } else {
        return reject('internal')
      }
    })
    .catch(error => reject(error))
})

export interface LockOrientationRequest extends Message {
  type: 'request',
  name: 'lockOrientation',
  payload: {
    orientation: Orientation
  }
}
export interface LockOrientationResponse extends Message {
  type: 'response',
  name: 'lockOrientation',
  error: MessageError | null
}
export const sendLockOrientationRequest = (orientation: Orientation) => new Promise<LockOrientationResponse>((resolve, reject) => {
  const request: LockOrientationRequest = {
    type: 'request',
    name: 'lockOrientation',
    payload: { orientation }
  }
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'lockOrientation')) {
        const { error } = message
        const response: LockOrientationResponse = {
          type: message.type,
          name: message.name,
          error: (error as MessageError) || null
        }
        return resolve(response)
      } else {
        return reject('internal')
      }
    })
    .catch(error => reject(error))
})
