import { Message, MessageError, sendRequest } from './index';

export interface GetEthereumAddressRequest extends Message {
  type: 'request',
  name: 'getEthereumAddress'
};
export interface GetEthereumAddressResponse extends Message {
  type: 'response',
  name: 'getEthereumAddress',
  payload: {
    ethereumAddress: string
  } | null,
  error: MessageError | null
};
export const sendGetEthereumAddressRequest = () => new Promise<GetEthereumAddressResponse>((resolve, reject) => {
  const request: GetEthereumAddressRequest = {
    type: 'request',
    name: 'getEthereumAddress'
  };
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'getEthereumAddress')) {
        const { payload, error } = message;
        const response: GetEthereumAddressResponse = {
          type: message.type,
          name: message.name,
          payload: payload || null,
          error: (error as MessageError) || null
        };
        return resolve(response);
      } else {
        return reject('internal');
      }
    })
    .catch(error => reject(error));
});

export interface RequestSignJWTRequest extends Message {
  type: 'request',
  name: 'requestSignJWT'
  payload: {
    serviceType: string | null
  }
};
export interface RequestSignJWTResponse extends Message {
  type: 'response',
  name: 'requestSignJWT',
  payload: {
    jwt: string
  } | null,
  error: MessageError | null
};
export const sendRequestSignJWTRequest = (serviceType?: string) => new Promise<RequestSignJWTResponse>((resolve, reject) => {
  const request: RequestSignJWTRequest = {
    type: 'request',
    name: 'requestSignJWT',
    payload: {
      serviceType: serviceType || null
    }
  };
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'requestSignJWT')) {
        const { payload, error } = message;
        const response: RequestSignJWTResponse = {
          type: message.type,
          name: message.name,
          payload: payload || null,
          error: (error as MessageError) || null
        };
        return resolve(response);
      } else {
        return reject('internal');
      }
    })
    .catch(error => reject(error));
});
