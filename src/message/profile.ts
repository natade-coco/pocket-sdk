import { Message, MessageError, sendRequest } from './index';

export interface GetProfileRequest extends Message {
  type: 'request',
  name: 'getProfile'
};
export interface GetProfileResponse extends Message {
  type: 'response',
  name: 'getProfile',
  payload: {
    profile: {
      name: string,
      fullName: string | null,
      email: string | null,
      tel: string | null,
      image: string | null
    } | null
  } | null,
  error: MessageError | null
};
export const sendGetProfileRequest = (params: string[]) => new Promise<GetProfileResponse>((resolve, reject) => {
  const request: GetProfileRequest = {
    type: 'request',
    name: 'getProfile',
    payload: { params }
  };
  sendRequest(request)
    .then(message => {
      if ((message.type === 'response') && (message.name === 'getProfile')) {
        const { payload, error } = message;
        const response: GetProfileResponse = {
          type: message.type,
          name: message.name,
          payload: payload || null,
          error: (error as MessageError) || null
        };
        return resolve(response);
      } else {
        return reject('internal');
      }
    })
    .catch(error => reject(error));
});
