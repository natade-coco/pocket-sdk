import * as Message from '../../message';
import * as Identity from '../../identity';
import { GetEthereumAddressResponse, RequestSignJWTResponse, RequestSignJWTRequest } from '../../message/identity';

describe('Identity', () => {
  const sendRequestSpy = jest.spyOn(Message, 'sendRequest');

  test('Get EthereumAddress (SUCCESS)', () => {
    // Arrange
    const dummyEthereumAddress = '0x1a2b3c';
    const dummyResponse: GetEthereumAddressResponse = {
      type: 'response',
      name: 'getEthereumAddress',
      payload: {
        ethereumAddress: dummyEthereumAddress
      },
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Identity.getEthereumAddress());

    // Assert
    result.resolves.toEqual(dummyEthereumAddress);
  });

  test('Get EthereumAddress (ERROR)', () => {
    // Arrange
    const dummyError = 'permissionDenied';
    const dummyResponse: GetEthereumAddressResponse = {
      type: 'response',
      name: 'getEthereumAddress',
      payload: null,
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Identity.getEthereumAddress());

    // Assert
    result.rejects.toBe(dummyError);
  });

  test('Request SignJWT (SUCCESS)', () => {
    // Arrange
    const dummyJWT = 'some-jwt';
    const dummyResponse: RequestSignJWTResponse = {
      type: 'response',
      name: 'requestSignJWT',
      payload: {
        jwt: dummyJWT
      },
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Identity.requestSignJWT());

    // Assert
    result.resolves.toEqual(dummyJWT);
  });

  test('Request SignJWT (ERROR)', () => {
    // Arrange
    const dummyError = 'permissionDenied';
    const dummyResponse: RequestSignJWTResponse = {
      type: 'response',
      name: 'requestSignJWT',
      payload: null,
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Identity.requestSignJWT());

    // Assert
    result.rejects.toBe(dummyError);
  });
});
