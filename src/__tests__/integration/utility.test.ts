import * as Message from '../../message';
import * as Utility from '../../utility';
import { OpenInBrowserResponse, ShareResponse } from '../../message/utility';
import { ShareContent } from '../../entities/utility';

const dummyShareContent: ShareContent = {
  text: 'Lorem Ipsum',
  file: { url: 'https://natade-coco.com/example.png', name: 'example.png', type: 'image/png' }
}

describe('Utility', () => {
  const sendRequestSpy = jest.spyOn(Message, 'sendRequest');

  test('Open in Browser (SUCCESS)', () => {
    // Arrange
    const dummyResponse: OpenInBrowserResponse = {
      type: 'response',
      name: 'openInBrowser',
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Utility.openInBrowser('https://example.com/'));

    // Assert
    result.resolves.toEqual(undefined);
  });

  test('Open in Browser (ERROR)', () => {
    // Arrange
    const dummyError = 'unavailable';
    const dummyResponse: OpenInBrowserResponse = {
      type: 'response',
      name: 'openInBrowser',
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Utility.openInBrowser('https://example.com/'));

    // Assert
    result.rejects.toBe(dummyError);
  });

  test('Share (SUCCESS)', () => {
    // Arrange
    const dummyResponse: ShareResponse = {
      type: 'response',
      name: 'share',
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Utility.share(dummyShareContent));

    // Assert
    result.resolves.toEqual(undefined);
  });

  test('Share (ERROR)', () => {
    // Arrange
    const dummyError = 'unavailable';
    const dummyResponse: ShareResponse = {
      type: 'response',
      name: 'share',
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Utility.share(dummyShareContent));

    // Assert
    result.rejects.toBe(dummyError);
  });
});
