import * as Message from '../../message';
import * as Connection from '../../connection';
import { MakeConnectionResponse } from '../../message/connection';

describe('Connection', () => {
  const sendRequestSpy = jest.spyOn(Message, 'sendRequest');

  test('Make Connection (SUCCESS)', () => {
    // Arrange
    const dummyURL = 'https://id.natade-coco.net/docs/0x1234';
    const dummyResponse: MakeConnectionResponse = {
      type: 'response',
      name: 'makeConnection',
      payload: {
        url: dummyURL
      },
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Connection.makeConnection('0x5678', '/some/path', ['0xabcd']));

    // Assert
    result.resolves.toEqual(dummyURL);
  });

  test('Make Connection (ERROR)', () => {
    // Arrange
    const dummyError = 'unavailable';
    const dummyResponse: MakeConnectionResponse = {
      type: 'response',
      name: 'makeConnection',
      payload: null,
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Connection.makeConnection('0x5678', '/some/path', ['0xabcd']));

    // Assert
    result.rejects.toBe(dummyError);
  });
});
