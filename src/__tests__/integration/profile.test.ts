import * as Message from '../../message';
import * as Profile from '../../profile';
import { GetProfileResponse } from '../../message/profile';

describe('Profile', () => {
  const sendRequestSpy = jest.spyOn(Message, 'sendRequest');

  test('Get Profile (SUCCESS)', () => {
    // Arrange
    const dummyProfile = {
      name: '名無しユーザー',
      fullName: 'ナタデココ 太郎',
      email: 'example@natade-coco.com',
      tel: '012-345-6789',
      image: 'https://natade-coco.com/example.png'
    };
    const dummyResponse: GetProfileResponse = {
      type: 'response',
      name: 'getProfile',
      payload: {
        profile: dummyProfile
      },
      error: null
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Profile.getProfile(Object.keys(dummyProfile)));

    // Assert
    result.resolves.toEqual(dummyProfile);
  });

  test('Get Profile (ERROR)', () => {
    // Arrange
    const dummyError = 'permissionDenied';
    const dummyResponse: GetProfileResponse = {
      type: 'response',
      name: 'getProfile',
      payload: null,
      error: dummyError
    };
    sendRequestSpy.mockResolvedValueOnce(dummyResponse);

    // Act
    const result = expect(Profile.getProfile(Object.keys(dummyResponse)));

    // Assert
    result.rejects.toBe(dummyError);
  });
});
