import { sendGetEthereumAddressRequest, sendRequestSignJWTRequest } from "./message/identity";

export const getEthereumAddress = () => new Promise<string>((resolve, reject) => {
  sendGetEthereumAddressRequest()
    .then(response => {
      const { payload, error } = response;
      if (!error && payload) {
        return resolve(payload.ethereumAddress);
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});

export const requestSignJWT = (serviceType?: string) => new Promise<string>((resolve, reject) => {
  sendRequestSignJWTRequest(serviceType)
    .then(response => {
      const { payload, error } = response;
      if (!error && payload) {
        return resolve(payload.jwt);
      } else {
        return reject(error);
      }
    })
    .catch(error => {
      return reject(error);
    });
});
