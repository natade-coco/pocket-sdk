export interface ShareContent {
  text?: string,
  file?: {
    url: string,
    name?: string,
    type?: string
  }
}

export type Orientation = 'portrait' | 'landscape' | 'landscapeLeft' | 'landscapeRight' | null
