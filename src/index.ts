import * as Connection from './connection'
import * as Identity from './identity'
import * as Profile from './profile'
import * as Utility from './utility'

export default {
  ...Connection,
  ...Identity,
  ...Profile,
  ...Utility
}

export * from './entities'
